import './App.css';
import {useEffect, useState} from 'react'

function App() {

  const [data, setData] = useState(null)

  async function fetchData() {
    const response = await await fetch("/api");
    const data = await response.json()
    setData(data.content)
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <p>{!data ? "Loading..." : data}</p>
      </header>
    </div>
  );
}

export default App;
