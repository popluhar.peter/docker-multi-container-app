const express = require("express")
const app = express()
const PORT = process.env.PORT || 5000

app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`)
})

app.get('/api', (req, res) => {
    res.send({ content: 'YOUR EXPRESS BACKEND IS CONNECTED TO YOUR FRONTEND REACT APP' });
})